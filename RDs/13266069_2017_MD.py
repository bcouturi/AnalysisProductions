
from GaudiConf import IOHelper

IOHelper().inputFiles(['root://x509up_u34662@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/2018/RXCHAD.STRIP.DST/00108343/0000/00108343_00000020_1.rxchad.strip.dst', 
'root://x509up_u34662@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/RXCHAD.STRIP.DST/00108346/0000/00108346_00000013_1.rxchad.strip.dst',
'root://castor-stager.gridpp.rl.ac.uk//castor/ads.rl.ac.uk/prod/lhcb/archive/lhcb/MC/2018/RXCHAD.STRIP.DST/00108343/0000/00108343_00000012_1.rxchad.strip.dst?svcClass=lhcbRawRdst'], clear=True)